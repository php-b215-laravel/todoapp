@extends('layouts.app')
@section('content')
<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">

    <title>ToDoApp</title>
</head>
<body>
<div class="row justify-content-center">

    <div class="text-center mt-5">

        <form class="row g-3 justify-content-center" method="POST" action="{{route('tasks.index')}}">
            @csrf
            <div class="col-3">
                <input type="text" class="form-control" name="name" placeholder="Add Task">
            </div>
            <div class="col-auto">
                <button type="submit" class="btn btn-success mb-3">Submit</button>
            </div>
        </form>
    </div>



    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>


    <div class="text-center">
    <div class="row justify-content-center">
        <div class="col-lg-6 mt-4">

            <table class="table table-bordered">
                <thead>
                <tr>
                    <th scope="col">Task</th>
                    <th scope="col">Status</th>
                    <th scope="col">Action</th>
                </tr>
                </thead>
                <tbody>

                @php $counter=1 @endphp

                @foreach($tasks as $task)
                    <tr>
                        <td>{{$task->name}}</td>
                        <td>
                            @if($task->is_completed)
                                <div class="badge bg-success">Completed</div>
                            @else
                                <div class="badge bg-warning">Incomplete</div>
                            @endif
                        </td>
                        <td>
                            <a href="{{route('tasks.edit',['task'=>$task->id])}}" class="btn btn-info">Edit</a>
                            <a href="{{route('tasks.destroy',['task'=>$task->id])}}" class="btn btn-danger">Delete</a>
                        </td>
                    </tr>

                    @php $counter++; @endphp

                @endforeach
                </tbody>
            </table>
        </div>
    </div>
    </div>

</body>
</html>  
@endsection


